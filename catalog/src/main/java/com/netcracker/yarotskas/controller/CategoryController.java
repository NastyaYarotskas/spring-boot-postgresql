package com.netcracker.yarotskas.controller;

import com.netcracker.yarotskas.dto.CategoryDto;
import com.netcracker.yarotskas.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "api/v1/categories")
public class CategoryController {

    private final CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping(path = "/{id}")
    public CategoryDto findById(@PathVariable("id") Long id) {
        return categoryService.findById(id);
    }

    @PostMapping
    public CategoryDto create(@RequestBody CategoryDto categoryDto) {
        return categoryService.create(categoryDto);
    }

    @PostMapping(value = "/all")
    public List<CategoryDto> createAll(@RequestBody List<CategoryDto> categoryDtoList) {
        return categoryService.createAll(categoryDtoList);
    }

    @PutMapping
    public CategoryDto update(@RequestBody CategoryDto category) {
        return categoryService.update(category);
    }

    @DeleteMapping(path = "/{id}")
    public void deleteById(@PathVariable("id") Long id) {
        categoryService.deleteById(id);
    }

    @GetMapping
    public List<CategoryDto> findAll() {
        return categoryService.findAll();
    }
}
