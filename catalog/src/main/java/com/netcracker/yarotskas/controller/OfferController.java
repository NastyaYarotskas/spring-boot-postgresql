package com.netcracker.yarotskas.controller;

import com.netcracker.yarotskas.dto.OfferDto;
import com.netcracker.yarotskas.service.OfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import java.util.List;
import java.util.Set;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "api/v1/offers")
public class OfferController {

    private final OfferService offerService;

    @Autowired
    public OfferController(OfferService offerService) {
        this.offerService = offerService;
    }

    @GetMapping(path = "/{id}")
    public OfferDto findById(@PathVariable("id") Long id) {
        return offerService.findById(id);
    }

    @PostMapping
    public OfferDto create(@RequestBody OfferDto offerDto) {
        return offerService.create(offerDto);
    }

    @GetMapping
    public List<OfferDto> findAll() {
        return offerService.findAll();
    }

    @PutMapping
    public OfferDto update(@RequestBody OfferDto offerToUpdate) {
        return offerService.update(offerToUpdate);
    }

    @DeleteMapping(path = "/{id}")
    public void deleteById(@PathVariable("id") Long id) {
        offerService.deleteById(id);
    }

    @PutMapping(path = "/{offerId}/tag/{tagName}")
    public OfferDto addTag(@PathVariable("offerId") Long offerId, @PathVariable("tagName") String tagName) {
        return offerService.addTag(offerId, tagName);
    }

    @DeleteMapping(path = "/{offerId}/tag/{tagName}")
    public OfferDto deleteTag(@PathVariable("offerId") Long offerId, @PathVariable("tagName") String tagName) {
        return offerService.deleteTag(offerId, tagName);
    }

    @PutMapping(path = "/{offerId}/category/{categoryName}")
    public OfferDto updateCategory(@PathVariable("offerId") Long offerId,
                                   @PathVariable("categoryName") String categoryName) {
        return offerService.updateCategory(offerId, categoryName);
    }

    @GetMapping(path = "/filter")
    public List<OfferDto> findOffersByFilter(@RequestParam(value = "tags", required = false) Set<String> tags,
                                             @RequestParam(value = "minPrice", required = false) @Digits(integer = 20, fraction = 0, message = "not a digit") String minPrice,
                                             @RequestParam(value = "maxPrice", required = false) @Digits(integer = 20, fraction = 0, message = "not a digit") String maxPrice,
                                             @RequestParam(value = "category", required = false) String categoryName) {
        return offerService.findOffersByFilter(tags, minPrice, maxPrice, categoryName);
    }
}
