package com.netcracker.yarotskas.controller;

import com.netcracker.yarotskas.dto.TagDto;
import com.netcracker.yarotskas.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "api/v1/tags")
public class TagController {

    private final TagService tagService;

    @Autowired
    public TagController(TagService tagService) {
        this.tagService = tagService;
    }

    @GetMapping(path = "/{id}")
    public TagDto findById(@PathVariable("id") Long id) {
        return tagService.findById(id);
    }

    @PostMapping
    public TagDto create(@RequestBody TagDto tagDto) {
        return tagService.create(tagDto);
    }

    @PostMapping(value = "/all")
    public List<TagDto> createAll(@RequestBody List<TagDto> tagDtoList) {
        return tagService.createAll(tagDtoList);
    }

    @PutMapping
    public TagDto update(@RequestBody TagDto tagToUpdate) {
        return tagService.update(tagToUpdate);
    }

    @DeleteMapping(path = "/{id}")
    public void deleteById(@PathVariable("id") Long id) {
        tagService.deleteById(id);
    }
}
