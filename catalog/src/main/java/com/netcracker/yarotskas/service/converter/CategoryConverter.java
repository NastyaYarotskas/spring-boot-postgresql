package com.netcracker.yarotskas.service.converter;

import com.netcracker.yarotskas.dto.CategoryDto;
import com.netcracker.yarotskas.entity.Category;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public interface CategoryConverter {

    Category convertFromDto(CategoryDto categoryDto);

    CategoryDto convertFromEntity(Category category);

    default List createFromDtos(Collection<CategoryDto> dtos) {
        return dtos.stream().map(this::convertFromDto).collect(Collectors.toList());
    }

    default List createFromEntities(Collection<Category> entities) {
        return entities.stream().map(this::convertFromEntity).collect(Collectors.toList());
    }

}
