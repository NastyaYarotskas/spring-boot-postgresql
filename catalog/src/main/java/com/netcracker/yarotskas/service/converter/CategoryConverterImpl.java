package com.netcracker.yarotskas.service.converter;

import com.netcracker.yarotskas.dto.CategoryDto;
import com.netcracker.yarotskas.entity.Category;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class CategoryConverterImpl implements CategoryConverter {
    @Override
    public Category convertFromDto(CategoryDto categoryDto) {
        Category category = new Category();
        BeanUtils.copyProperties(categoryDto, category);
        return category;
    }

    @Override
    public CategoryDto convertFromEntity(Category category) {
        CategoryDto categoryDto = new CategoryDto();
        BeanUtils.copyProperties(category, categoryDto);
        return categoryDto;
    }
}
