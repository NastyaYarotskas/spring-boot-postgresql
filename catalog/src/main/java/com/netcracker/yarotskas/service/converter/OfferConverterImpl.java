package com.netcracker.yarotskas.service.converter;

import com.netcracker.yarotskas.dto.OfferDto;
import com.netcracker.yarotskas.entity.Category;
import com.netcracker.yarotskas.entity.Offer;
import com.netcracker.yarotskas.entity.Price;
import com.netcracker.yarotskas.entity.Tag;
import com.netcracker.yarotskas.repository.CategoryRepository;
import com.netcracker.yarotskas.repository.TagRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class OfferConverterImpl implements OfferConverter {

    private final TagRepository tagRepository;

    private final CategoryRepository categoryRepository;

    public OfferConverterImpl(TagRepository tagRepository, CategoryRepository categoryRepository) {
        this.tagRepository = tagRepository;
        this.categoryRepository = categoryRepository;
    }

    @Override
    public Offer convertFromDto(OfferDto offerDto) {
        Offer offer = new Offer();
        BeanUtils.copyProperties(offerDto, offer);
        List<Tag> tags = offerDto.getTags().stream()
                .map(tagDtoName -> {
                    Tag tag = tagRepository.findFirstByName(tagDtoName);
                    if (tag == null) {
                        tag = new Tag();
                        tag.setName(tagDtoName);
                    }

                    tag.getOffers().add(offer);
                    return tag;
                })
                .collect(Collectors.toList());

        offer.setTags(tags);

        Category category = categoryRepository.findFirstByName(offerDto.getCategory());
        if (category == null) {
            category = new Category();
            category.setName(offerDto.getCategory());
            category.getOffers().add(offer);
        }

        offer.setCategory(category);

        Price price = new Price();
        price.setValue(offerDto.getPrice());
        price.setOffer(offer);
        offer.setPrice(price);

        return offer;
    }

    @Override
    public OfferDto convertFromEntity(Offer offer) {
        OfferDto offerDto = new OfferDto();
        BeanUtils.copyProperties(offer, offerDto);
        offerDto.setPrice(offer.getPrice().getValue());
        offerDto.setCategory(offer.getCategory().getName());

        List<String> tags = offer.getTags().stream()
                .map(tag -> tag.getName())
                .collect(Collectors.toList());

        offerDto.setTags(tags);

        return offerDto;
    }

}
