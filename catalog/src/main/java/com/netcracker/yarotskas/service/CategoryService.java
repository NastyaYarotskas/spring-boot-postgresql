package com.netcracker.yarotskas.service;

import com.netcracker.yarotskas.dto.CategoryDto;
import com.netcracker.yarotskas.entity.Category;

import java.util.List;

public interface CategoryService {

    CategoryDto create(CategoryDto categoryDto);

    List<CategoryDto> createAll(List<CategoryDto> categoryDtos);

    CategoryDto findById(Long id);

    CategoryDto update(CategoryDto categoryDto);

    void deleteById(Long id);

    List<CategoryDto> findAll();
}
