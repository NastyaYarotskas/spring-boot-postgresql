package com.netcracker.yarotskas.service.converter;

import com.netcracker.yarotskas.dto.TagDto;
import com.netcracker.yarotskas.entity.Tag;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public interface TagConverter {

    Tag convertFromDto(TagDto tagDto);

    TagDto convertFromEntity(Tag tag);

    default List<Tag> createFromDtos(Collection<TagDto> dtos) {
        return dtos.stream().map(this::convertFromDto).collect(Collectors.toList());
    }

    default List<TagDto> createFromEntities(Collection<Tag> entities) {
        return entities.stream().map(this::convertFromEntity).collect(Collectors.toList());
    }

}
