package com.netcracker.yarotskas.service.impl;

import com.netcracker.yarotskas.dto.CategoryDto;
import com.netcracker.yarotskas.entity.Category;
import com.netcracker.yarotskas.exception.EntityNotFoundException;
import com.netcracker.yarotskas.repository.CategoryRepository;
import com.netcracker.yarotskas.service.CategoryService;
import com.netcracker.yarotskas.service.converter.CategoryConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;

    private final CategoryConverter categoryConverter;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository, CategoryConverter categoryConverter) {
        this.categoryRepository = categoryRepository;
        this.categoryConverter = categoryConverter;
    }

    @Override
    public CategoryDto create(CategoryDto categoryDto) {
        Category category = categoryConverter.convertFromDto(categoryDto);
        category = categoryRepository.save(category);
        return categoryConverter.convertFromEntity(category);
    }

    @Override
    public List<CategoryDto> createAll(List<CategoryDto> categoryDtos) {
        List<Category> categories = categoryConverter.createFromDtos(categoryDtos);
        categories = (List<Category>) categoryRepository.saveAll(categories);
        return categoryConverter.createFromEntities(categories);
    }

    @Override
    public CategoryDto findById(Long id) {
        Category category = categoryRepository.findById(id).orElseThrow(NullPointerException::new);
        return categoryConverter.convertFromEntity(category);
    }

    @Override
    public CategoryDto update(CategoryDto categoryDto) {
        Category category = categoryConverter.convertFromDto(categoryDto);
        category = categoryRepository.save(category);
        return categoryConverter.convertFromEntity(category);
    }

    @Override
    public void deleteById(Long id) {
        try {
            categoryRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }

    @Override
    public List<CategoryDto> findAll() {
        return categoryConverter.createFromEntities((Collection<Category>) categoryRepository.findAll());
    }
}
