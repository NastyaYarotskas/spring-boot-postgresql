package com.netcracker.yarotskas.service;

import com.netcracker.yarotskas.dto.TagDto;
import com.netcracker.yarotskas.entity.Tag;

import java.util.List;

public interface TagService {

    TagDto create(TagDto tagDto);

    List<TagDto> createAll(List<TagDto> tagDtos);

    TagDto findById(Long id);

    TagDto update(TagDto tag);

    void deleteById(Long id);

}
