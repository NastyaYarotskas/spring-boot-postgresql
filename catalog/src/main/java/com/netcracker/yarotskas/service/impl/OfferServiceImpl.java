package com.netcracker.yarotskas.service.impl;

import com.netcracker.yarotskas.dto.OfferDto;
import com.netcracker.yarotskas.entity.Category;
import com.netcracker.yarotskas.entity.Offer;
import com.netcracker.yarotskas.entity.Tag;
import com.netcracker.yarotskas.exception.EntityNotFoundException;
import com.netcracker.yarotskas.repository.CategoryRepository;
import com.netcracker.yarotskas.repository.OfferRepository;
import com.netcracker.yarotskas.repository.TagRepository;
import com.netcracker.yarotskas.service.OfferService;
import com.netcracker.yarotskas.service.converter.OfferConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class OfferServiceImpl implements OfferService {

    private final OfferRepository offerRepository;

    private final CategoryRepository categoryRepository;

    private final TagRepository tagRepository;

    private final OfferConverter offerConverter;

    @Autowired
    public OfferServiceImpl(OfferRepository offerRepository, CategoryRepository categoryRepository,
                            TagRepository tagRepository, OfferConverter offerConverter) {
        this.offerRepository = offerRepository;
        this.categoryRepository = categoryRepository;
        this.tagRepository = tagRepository;
        this.offerConverter = offerConverter;
    }

    @Override
    public OfferDto create(OfferDto offerDto) {
        Offer offer = offerConverter.convertFromDto(offerDto);
        offer = offerRepository.save(offer);
        return offerConverter.convertFromEntity(offer);
    }

    @Override
    public List<OfferDto> findAll() {
        return offerConverter.createFromEntities((Collection<Offer>) offerRepository.findAll());
    }

    @Override
    public OfferDto findById(Long id) {
        Offer offer = offerRepository.findById(id).orElseThrow(NullPointerException::new);
        return offerConverter.convertFromEntity(offer);
    }

    @Override
    public OfferDto update(OfferDto offerDto) {
        Offer offer = offerConverter.convertFromDto(offerDto);
        offer = offerRepository.save(offer);
        return offerConverter.convertFromEntity(offer);
    }

    @Override
    public void deleteById(Long id) {
        try {
            offerRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }

    @Override
    public OfferDto updateCategory(Long offerId, String categoryName) {
        Offer changedOffer = offerRepository.findById(offerId).orElseThrow(NullPointerException::new);
        Category newCategory = categoryRepository.findFirstByName(categoryName);
        if (newCategory == null) {
            newCategory = new Category();
            newCategory.setName(categoryName);
            newCategory.setOffers(Collections.singletonList(changedOffer));
        }
        changedOffer.setCategory(newCategory);
        return offerConverter.convertFromEntity(offerRepository.save(changedOffer));
    }

    @Override
    public OfferDto addTag(Long offerId, String tagName) {
        Offer changedOffer = offerRepository.findById(offerId).orElseThrow(NullPointerException::new);
        Tag tag = tagRepository.findFirstByName(tagName);
        if (tag == null) {
            tag = new Tag();
            tag.setName(tagName);
            tag.setOffers(Collections.singleton(changedOffer));
        }
        if (!changedOffer.getTags().contains(tag)) {
            changedOffer.getTags().add(tag);
        }

        return offerConverter.convertFromEntity(offerRepository.save(changedOffer));
    }

    @Override
    public OfferDto deleteTag(Long offerId, String tagName) {
        Offer changedOffer = offerRepository.findById(offerId).orElseThrow(NullPointerException::new);
        Tag tag = tagRepository.findFirstByName(tagName);
        changedOffer.getTags().remove(tag);
        return offerConverter.convertFromEntity(offerRepository.save(changedOffer));
    }

    @Override
    public List<OfferDto> findOffersByFilter(Set<String> tags, String minPriceValue, String maxPriceValue, String categoryName) {
        List<Offer> result;

        List<Tag> tagList = new ArrayList<>();

        if (tags != null) {
            tagList = tags.stream()
                    .map(tagName -> {
                        Tag tag = tagRepository.findFirstByName(tagName);
                        return tag;
                    }).collect(Collectors.toList());
        }

        Category category = categoryRepository.findFirstByName(categoryName);

        if (category != null && !tagList.isEmpty()) {
            result = offerRepository.findDistinctAllByCategoryAndTagsIn(category, tagList);
        } else if (!tagList.isEmpty()){
            result = offerRepository.findDistinctAllByTagsIn(tagList);
        } else {
            result = offerRepository.findByCategory(category);
        }

        if (minPriceValue != null ) {
            Double minPrice = Double.parseDouble(minPriceValue);

            if (category == null && tagList.isEmpty()) {
                result = (List<Offer>) offerRepository.findAll();
            }
            result = result.stream()
                    .filter(offer -> {
                        if (offer.getPrice().getValue() >= minPrice) {
                            return true;
                        }

                        return false;
                    })
                    .collect(Collectors.toList());
        }

        if (maxPriceValue != null ) {
            Double maxPrice = Double.parseDouble(maxPriceValue);

            if (category == null && tagList.isEmpty() && minPriceValue == null) {
                result = (List<Offer>) offerRepository.findAll();
            }

            result = result.stream()
                    .filter(offer -> {
                        if (offer.getPrice().getValue() <= maxPrice) {
                            return true;
                        }

                        return false;
                    })
                    .collect(Collectors.toList());
        }


        return offerConverter.createFromEntities(result);
    }



}
