package com.netcracker.yarotskas.service.impl;

import com.netcracker.yarotskas.dto.TagDto;
import com.netcracker.yarotskas.entity.Tag;
import com.netcracker.yarotskas.exception.EntityNotFoundException;
import com.netcracker.yarotskas.repository.TagRepository;
import com.netcracker.yarotskas.service.TagService;
import com.netcracker.yarotskas.service.converter.TagConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class TagServiceImpl implements TagService {

    private final TagRepository tagRepository;

    private final TagConverter tagConverter;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository, TagConverter tagConverter) {
        this.tagRepository = tagRepository;
        this.tagConverter = tagConverter;
    }

    @Override
    public TagDto create(TagDto tagDto) {
        Tag tag = tagConverter.convertFromDto(tagDto);
        tag = tagRepository.save(tag);
        return tagConverter.convertFromEntity(tag);
    }

    @Override
    public List<TagDto> createAll(List<TagDto> tagDtos) {
        List<Tag> tags = tagConverter.createFromDtos(tagDtos);
        tags = (List<Tag>) tagRepository.saveAll(tags);
        return tagConverter.createFromEntities(tags);
    }

    @Override
    public TagDto findById(Long id) {
        Tag tag = tagRepository.findById(id).orElseThrow(NullPointerException::new);
        return tagConverter.convertFromEntity(tag);
    }

    @Override
    public TagDto update(TagDto tagDto) {
        Tag tag = tagConverter.convertFromDto(tagDto);
        tag = tagRepository.save(tag);
        return tagConverter.convertFromEntity(tag);
    }

    @Override
    public void deleteById(Long id) {
        try {
            tagRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }
}
