package com.netcracker.yarotskas.service.converter;

import com.netcracker.yarotskas.dto.TagDto;
import com.netcracker.yarotskas.entity.Tag;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class TagConverterImpl implements TagConverter {

    @Override
    public Tag convertFromDto(TagDto tagDto) {
        Tag tag = new Tag();
        BeanUtils.copyProperties(tagDto, tag);
        return tag;
    }

    @Override
    public TagDto convertFromEntity(Tag tag) {
        TagDto tagDto = new TagDto();
        BeanUtils.copyProperties(tag, tagDto);
        return tagDto;
    }

}
