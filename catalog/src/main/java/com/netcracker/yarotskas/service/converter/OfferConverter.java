package com.netcracker.yarotskas.service.converter;

import com.netcracker.yarotskas.dto.OfferDto;
import com.netcracker.yarotskas.entity.Offer;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public interface OfferConverter {

    Offer convertFromDto(OfferDto offerDto);

    OfferDto convertFromEntity(Offer offer);

    default List createFromDtos(Collection<OfferDto> dtos) {
        return dtos.stream().map(this::convertFromDto).collect(Collectors.toList());
    }

    default List createFromEntities(Collection<Offer> entities) {
        return entities.stream().map(this::convertFromEntity).collect(Collectors.toList());
    }

}
