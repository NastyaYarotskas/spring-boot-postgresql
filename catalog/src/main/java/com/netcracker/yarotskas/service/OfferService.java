package com.netcracker.yarotskas.service;

import com.netcracker.yarotskas.dto.OfferDto;

import java.util.List;
import java.util.Set;

public interface OfferService {

    OfferDto create(OfferDto offerDto);

    List<OfferDto> findAll();

    OfferDto findById(Long id);

    OfferDto update(OfferDto offerDto);

    void deleteById(Long id);

    OfferDto updateCategory(Long offerId, String categoryName);

    OfferDto addTag(Long offerId, String tagName);

    OfferDto deleteTag(Long offerId, String tagName);

    List<OfferDto> findOffersByFilter(Set<String> tags, String minPrice, String maxPrice, String categoryName);
}
