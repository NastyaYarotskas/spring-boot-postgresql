package com.netcracker.yarotskas.entity;

import com.google.common.base.Objects;
import lombok.Data;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Data
@Entity
public class Price extends BaseEntity {

    @Basic(optional = false)
    private Double value;

    @OneToOne
    Offer offer;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Price price = (Price) o;
        return Objects.equal(value, price.value) &&
                Objects.equal(offer, price.offer);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(super.hashCode(), value);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Price{");
        sb.append("id='").append(super.getId()).append('\'');
        sb.append("value=").append(value);
        sb.append('}');
        return sb.toString();
    }
}
