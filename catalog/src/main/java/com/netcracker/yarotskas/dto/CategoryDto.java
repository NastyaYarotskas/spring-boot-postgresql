package com.netcracker.yarotskas.dto;

import lombok.Data;

@Data
public class CategoryDto implements BaseEntityDto {

    private Long id;

    private String name;

}
