package com.netcracker.yarotskas.dto;

import lombok.Data;

@Data
public class PriceDto implements BaseEntityDto {

    private Long id;

    private Double value;

}
