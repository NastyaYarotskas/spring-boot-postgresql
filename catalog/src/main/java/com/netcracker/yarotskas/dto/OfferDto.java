package com.netcracker.yarotskas.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class OfferDto implements BaseEntityDto {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private Double price;

    private String category;

    private List<String> tags;

    private String imageUrl;

    private String description;
}
