package com.netcracker.yarotskas.repository;

import com.netcracker.yarotskas.entity.Category;
import com.netcracker.yarotskas.entity.Offer;
import com.netcracker.yarotskas.entity.Price;
import com.netcracker.yarotskas.entity.Tag;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OfferRepository extends CrudRepository<Offer, Long> {

    List findDistinctAllByTagsIn(List<Tag> tags);

    List findByCategory(Category category);

    List findDistinctAllByCategoryAndTagsIn(Category category, List<Tag> tags);

}
