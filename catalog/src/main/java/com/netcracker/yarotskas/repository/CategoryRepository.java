package com.netcracker.yarotskas.repository;

import com.netcracker.yarotskas.entity.Category;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends CrudRepository<Category, Long> {

    Category findFirstByName(String name);

}
