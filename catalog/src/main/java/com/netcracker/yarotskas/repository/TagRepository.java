package com.netcracker.yarotskas.repository;

import com.netcracker.yarotskas.entity.Tag;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TagRepository extends CrudRepository<Tag, Long> {

    Tag findFirstByName(String name);

}
