package com.netcracker.yarotskas.controller;

import com.netcracker.yarotskas.dto.inventory.AddressDto;
import com.netcracker.yarotskas.dto.inventory.OrderDto;
import com.netcracker.yarotskas.dto.processor.OrderFormDto;
import com.netcracker.yarotskas.dto.processor.OrderItemFormDto;
import com.netcracker.yarotskas.dto.processor.PayFormDto;
import com.netcracker.yarotskas.service.ProcessorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "/api/v1/processor")
public class ProcessorController {

    private final ProcessorService processorService;

    @Autowired
    public ProcessorController(ProcessorService processorService) {
        this.processorService = processorService;
    }

    @PostMapping(value = "/order")
    private OrderDto createOrder(@RequestBody OrderFormDto orderFormDto) {
        return  processorService.createOrder(orderFormDto);
    }

    @PostMapping(value = "/item")
    private OrderDto addOrderItem(@RequestBody OrderItemFormDto orderFormDto) {
        return  processorService.addOrderItem(orderFormDto);
    }

    @GetMapping(value = "/totalPrice/{email}")
    private Double getTotalOrderPrice(@PathVariable("email") String email) {
        return processorService.getTotalOrderPrice(email);
    }

    @GetMapping(value = "/amount/{email}")
    private Integer getCount(@PathVariable("email") String email) {
        return processorService.getOrderCount(email);
    }

    @GetMapping(value = "/email/{email}")
    private List<OrderDto> getOrdersByEmail(@PathVariable("email") String email) {
        return processorService.getOrdersByEmail(email);
    }

    @GetMapping(value = "/order/paid/{paid}")
    private List<OrderDto> getOrderByPaidStatus(@PathVariable("paid") boolean paid) {
        return processorService.getOrderByPaidStatus(paid);
    }

    @GetMapping(value = "/order/{orderId}")
    public OrderDto getOrder(@PathVariable("orderId") Long orderId) {
        return processorService.getOrder(orderId);
    }

    @PostMapping(path = "/pay")
    public OrderDto payForOrder(@RequestBody PayFormDto payFormDto) {
        Long id = Long.valueOf(payFormDto.getOrderId());
        return processorService.payForOrder(id);
    }

    @DeleteMapping(value = "/order/item/{orderItemId}")
    public OrderDto deleteOrderItem(@PathVariable("orderItemId") Long orderItemId) {
        return processorService.deleteOrderItem(orderItemId);
    }

    @PostMapping(path = "/address/{orderId}")
    public OrderDto setOrderAddress(@PathVariable("orderId") Long orderId, @RequestBody AddressDto addressDto) {
        return processorService.setOrderAddress(orderId, addressDto);
    }
}

