package com.netcracker.yarotskas.dto.processor;

import com.netcracker.yarotskas.dto.BaseEntityDto;
import com.netcracker.yarotskas.dto.inventory.AddressDto;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
public class OrderFormDto implements BaseEntityDto {

    private Long id;

    @Email
    private String email;

    @NotNull
    private AddressDto deliveryAddress;

    @NotNull
    private Long customerId;
}
