package com.netcracker.yarotskas.dto.catalog;

import com.netcracker.yarotskas.dto.BaseEntityDto;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class OfferDto implements BaseEntityDto {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    @Range(min = 0)
    private Double price;

    @NotNull
    private String category;

    @NotNull
    private List<String> tags;

    private String imageUrl;

    private String description;
}
