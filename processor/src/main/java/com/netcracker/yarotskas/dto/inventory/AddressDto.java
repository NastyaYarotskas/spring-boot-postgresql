package com.netcracker.yarotskas.dto.inventory;

import com.netcracker.yarotskas.dto.BaseEntityDto;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class AddressDto implements BaseEntityDto {

    private Long id;

    @NotNull
    private String street;

    @NotNull
    private String city;

}
