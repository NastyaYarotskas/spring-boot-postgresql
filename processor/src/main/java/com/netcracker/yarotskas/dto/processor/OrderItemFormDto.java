package com.netcracker.yarotskas.dto.processor;

import com.netcracker.yarotskas.dto.BaseEntityDto;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class OrderItemFormDto implements BaseEntityDto {

    private Long id;

    @NotNull
    private Long offerId;

    @NotNull
    private Long orderId;

    @NotNull
    @Range(min = 1)
    private Integer quantity;

    @NotNull
    private String name;

    @NotNull
    @Range(min = 0, message = "Please select positive numbers Only")
    private Double price;

    private String imageUrl;

    private String description;
}
