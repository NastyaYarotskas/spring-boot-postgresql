package com.netcracker.yarotskas.dto.inventory;

import com.netcracker.yarotskas.dto.BaseEntityDto;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class OrderItemDto implements BaseEntityDto {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    @Range(min = 1)
    private Integer quantity;

    @NotNull
    private Long offerId;

    @NotNull
    @Range(min = 0)
    private Double price;

    private String category;

    private List<String> tags;

    private String imageUrl;

    private String description;

    @NotNull
    private Long orderId;

}
