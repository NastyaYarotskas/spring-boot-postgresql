package com.netcracker.yarotskas.dto.inventory;

import com.netcracker.yarotskas.dto.BaseEntityDto;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class OrderDto implements BaseEntityDto {

    private Long id;

    @NotNull
    private List<OrderItemDto> orderItems;

    @NotNull
    private String status;

    @NotNull
    private AddressDto deliveryAddress;

    @NotNull
    private Long customerId;

    @NotNull
    @Range(min = 0)
    private Double totalPrice;

    @NotNull
    @Range(min = 0)
    private Integer orderItemCount;

    @NotNull
    private Date date;

    @NotNull
    private Boolean isPaid;

    @Email
    private String email;

}
