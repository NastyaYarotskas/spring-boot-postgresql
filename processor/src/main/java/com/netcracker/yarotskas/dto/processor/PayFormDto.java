package com.netcracker.yarotskas.dto.processor;

import com.netcracker.yarotskas.dto.BaseEntityDto;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class PayFormDto implements BaseEntityDto {

    @NotNull
    private Integer orderId;

}
