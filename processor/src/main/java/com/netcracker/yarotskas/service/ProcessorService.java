package com.netcracker.yarotskas.service;

import com.netcracker.yarotskas.dto.inventory.AddressDto;
import com.netcracker.yarotskas.dto.inventory.OrderDto;
import com.netcracker.yarotskas.dto.processor.OrderFormDto;
import com.netcracker.yarotskas.dto.processor.OrderItemFormDto;

import java.util.List;

public interface ProcessorService {

    OrderDto createOrder(OrderFormDto orderFormDto);

    OrderDto addOrderItem(OrderItemFormDto orderFormDto);

    Double getTotalOrderPrice(String email);

    Integer getOrderCount(String email);

    List<OrderDto> getOrdersByEmail(String email);

    List<OrderDto> getOrderByPaidStatus(boolean paid);

    OrderDto getOrder(Long orderId);

    OrderDto payForOrder(Long orderId);

    OrderDto deleteOrderItem(Long orderItemId);

    OrderDto setOrderAddress(Long orderId, AddressDto addressDto);
}
