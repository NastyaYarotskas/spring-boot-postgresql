package com.netcracker.yarotskas.service.converter;

import com.netcracker.yarotskas.dto.catalog.OfferDto;
import com.netcracker.yarotskas.dto.inventory.OrderItemDto;

public interface OfferDtoConverter {

    OrderItemDto convertFromOffer(OfferDto offerDto);

}
