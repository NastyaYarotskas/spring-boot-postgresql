package com.netcracker.yarotskas.service.client;

import com.netcracker.yarotskas.dto.catalog.OfferDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Component
public class CatalogClient {

    private static final String API_V1_OFFERS = "/api/v1/offers/";

    private final RestTemplate restTemplate;

    @Value("${catalog.url}")
    private String urlBase;

    public CatalogClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public OfferDto findOfferById(Long offerId) {
        StringBuilder finalUrl = new StringBuilder(urlBase);
        finalUrl.append(API_V1_OFFERS);
        finalUrl.append(offerId);

        ResponseEntity<OfferDto> responseEntity = restTemplate.exchange(finalUrl.toString(), HttpMethod.GET, getHeaders(), OfferDto.class);
        return responseEntity.getBody();
    }

    private HttpEntity<String> getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        return entity;
    }
}
