package com.netcracker.yarotskas.service.converter;

import com.netcracker.yarotskas.dto.catalog.OfferDto;
import com.netcracker.yarotskas.dto.inventory.OrderItemDto;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class OfferDtoConverterImpl implements OfferDtoConverter {
    @Override
    public OrderItemDto convertFromOffer(OfferDto offerDto) {
        OrderItemDto orderItemDto = new OrderItemDto();
        BeanUtils.copyProperties(offerDto, orderItemDto, "id");
        orderItemDto.setOfferId(offerDto.getId());
        return orderItemDto;
    }
}
