package com.netcracker.yarotskas.service.converter;

import com.netcracker.yarotskas.dto.inventory.OrderDto;
import com.netcracker.yarotskas.dto.processor.OrderFormDto;

public interface OrderFormConverter {

    OrderDto convertFromOrderForm(OrderFormDto orderFormDto);

}
