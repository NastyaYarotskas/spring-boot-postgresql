package com.netcracker.yarotskas.service.converter;

import com.netcracker.yarotskas.dto.inventory.OrderDto;
import com.netcracker.yarotskas.dto.processor.OrderFormDto;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class OrderFormConverterImpl implements OrderFormConverter {
    @Override
    public OrderDto convertFromOrderForm(OrderFormDto orderFormDto) {
        OrderDto orderDto = new OrderDto();
        orderDto.setEmail(orderFormDto.getEmail());
        orderDto.setDate(new Date());
        orderDto.setStatus("NEW");
        orderDto.setIsPaid(false);
        orderDto.setDeliveryAddress(orderFormDto.getDeliveryAddress());
        orderDto.setCustomerId(orderFormDto.getCustomerId());
        orderDto.setTotalPrice(0.0);
        orderDto.setOrderItemCount(0);
        orderDto.setId(0L);
        return orderDto;
    }
}
