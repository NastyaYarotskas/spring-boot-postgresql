package com.netcracker.yarotskas.service;

import com.netcracker.yarotskas.dto.catalog.OfferDto;
import com.netcracker.yarotskas.dto.inventory.AddressDto;
import com.netcracker.yarotskas.dto.inventory.OrderDto;
import com.netcracker.yarotskas.dto.inventory.OrderItemDto;
import com.netcracker.yarotskas.dto.processor.OrderFormDto;
import com.netcracker.yarotskas.dto.processor.OrderItemFormDto;
import com.netcracker.yarotskas.service.client.CatalogClient;
import com.netcracker.yarotskas.service.client.InventoryClient;
import com.netcracker.yarotskas.service.converter.OfferDtoConverter;
import com.netcracker.yarotskas.service.converter.OrderFormConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProcessorServiceImpl implements ProcessorService {

    private final CatalogClient catalogClient;

    private final InventoryClient inventoryClient;

    private final OfferDtoConverter converter;

    private final OrderFormConverter converterOrderForm;

    @Autowired
    public ProcessorServiceImpl(CatalogClient catalogClient, InventoryClient inventoryClient, OfferDtoConverter converter, OrderFormConverter converterOrderForm) {
        this.catalogClient = catalogClient;
        this.inventoryClient = inventoryClient;
        this.converter = converter;
        this.converterOrderForm = converterOrderForm;
    }

    @Override
    public OrderDto createOrder(OrderFormDto orderFormDto) {
        OrderDto orderDto = converterOrderForm.convertFromOrderForm(orderFormDto);
        orderDto = inventoryClient.create(orderDto);
        return orderDto;
    }

    @Override
    public OrderDto addOrderItem(OrderItemFormDto orderItemFormDto) {
        OfferDto offerDto = catalogClient.findOfferById(orderItemFormDto.getOfferId());
        OrderDto orderDto = inventoryClient.findOrderById(orderItemFormDto.getOrderId());
        if (offerDto.getId() != null && orderDto.getId() != 0) {
            OrderItemDto orderItemDto = converter.convertFromOffer(offerDto);
            orderItemDto.setQuantity(orderItemFormDto.getQuantity());
            orderItemDto.setOrderId(orderDto.getId());
            orderDto = inventoryClient.addItem(orderItemDto);
        }

        return orderDto;
    }

    @Override
    public Double getTotalOrderPrice(String email) {
        return inventoryClient.getOrderTotalPrice(email);
    }

    @Override
    public Integer getOrderCount(String email) {
        return inventoryClient.getOrderCount(email);
    }

    @Override
    public List<OrderDto> getOrdersByEmail(String email) {
        return inventoryClient.findOrderByEmail(email);
    }

    @Override
    public List<OrderDto> getOrderByPaidStatus(boolean paid) {
        return inventoryClient.getOrderByPaidStatus(paid);
    }

    @Override
    public OrderDto getOrder(Long orderId) {
        return inventoryClient.findOrderById(orderId);
    }

    @Override
    public OrderDto payForOrder(Long orderId) {
        return inventoryClient.payForOrder(orderId);
    }

    @Override
    public OrderDto deleteOrderItem(Long orderItemId) {
        return inventoryClient.deleteOrderItem(orderItemId);
    }

    @Override
    public OrderDto setOrderAddress(Long orderId, AddressDto addressDto) {
        return inventoryClient.setOrderAddress(orderId, addressDto);
    }
}
