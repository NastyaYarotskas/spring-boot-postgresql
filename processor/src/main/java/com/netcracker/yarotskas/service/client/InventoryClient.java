package com.netcracker.yarotskas.service.client;

import com.netcracker.yarotskas.dto.inventory.AddressDto;
import com.netcracker.yarotskas.dto.inventory.OrderDto;
import com.netcracker.yarotskas.dto.inventory.OrderItemDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Component
public class InventoryClient {

    private static final String API_V1_ORDERS = "/api/v1/orders";
    private static final String API_V1_ORDERS_ID = "/api/v1/orders/";
    private static final String API_V1_ORDERS_COUNT = "/api/v1/orders/count/";
    private static final String API_V1_ORDERS_TOTAL_PRICE = "/api/v1/orders/total/";
    private static final String API_V1_ORDERS_ADDRESS = "/api/v1/address/";
    private static final String API_V1_ORDERS_EMAIL = "/api/v1/orders/email/";
    private static final String API_V1_ORDERS_PAID_STATUS = "/api/v1/orders/order/";
    private static final String API_V1_ORDERS_PAID = "/api/v1/orders/pay";
    private static final String API_V1_ORDERS_DELETE_ITEM = "/api/v1/orders/order/";
    private static final String API_V1_ORDERS_ADD_ITEM = "/api/v1/orders/item";

    private final RestTemplate restTemplate;

    @Value("${inventory.url}")
    private String urlBase;

    @Autowired
    public InventoryClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public OrderDto create(OrderDto orderDto) {
        StringBuilder finalUrl = new StringBuilder(urlBase);
        finalUrl.append(API_V1_ORDERS);

        ResponseEntity<OrderDto> responseEntity = restTemplate.postForEntity(finalUrl.toString(), orderDto, OrderDto.class);
        return responseEntity.getBody();
    }

    public OrderDto addItem(OrderItemDto orderItemDto) {
        StringBuilder finalUrl = new StringBuilder(urlBase);
        finalUrl.append(API_V1_ORDERS_ADD_ITEM);

        ResponseEntity<OrderDto> responseEntity = restTemplate.exchange(finalUrl.toString(), HttpMethod.PUT,
                getHeaders(orderItemDto), OrderDto.class);
        return responseEntity.getBody();
    }

    public OrderDto findOrderById(Long id) {
        StringBuilder finalUrl = new StringBuilder(urlBase);
        finalUrl.append(API_V1_ORDERS_ID);
        finalUrl.append(id);

        ResponseEntity<OrderDto> responseEntity = restTemplate.exchange(finalUrl.toString(), HttpMethod.GET,
                getHeaders(), OrderDto.class);
        return responseEntity.getBody();
    }

    public List<OrderDto> findOrderByEmail(String email) {
        StringBuilder finalUrl = new StringBuilder(urlBase);
        finalUrl.append(API_V1_ORDERS_EMAIL);
        finalUrl.append(email);

        ResponseEntity<OrderDto[]> responseEntity = restTemplate.getForEntity(finalUrl.toString(),
                OrderDto[].class, email);
        return Arrays.asList(responseEntity.getBody());
    }

    public OrderDto payForOrder(Long orderId) {
        StringBuilder finalUrl = new StringBuilder(urlBase);
        finalUrl.append(API_V1_ORDERS_PAID);

        ResponseEntity<OrderDto> responseEntity = restTemplate.exchange(finalUrl.toString(), HttpMethod.PUT, getHeaders(orderId),
                OrderDto.class, orderId);
        return responseEntity.getBody();
    }

    public OrderDto deleteOrderItem(Long id) {
        StringBuilder finalUrl = new StringBuilder(urlBase);
        finalUrl.append(API_V1_ORDERS_DELETE_ITEM);
        finalUrl.append(id);

        ResponseEntity<OrderDto> responseEntity = restTemplate.exchange(finalUrl.toString(), HttpMethod.DELETE, getHeaders(), OrderDto.class);

        return responseEntity.getBody();
    }

    public List<OrderDto> getOrderByPaidStatus(boolean paid) {
        StringBuilder finalUrl = new StringBuilder(urlBase);
        finalUrl.append(API_V1_ORDERS_PAID_STATUS);
        finalUrl.append(paid);

        ResponseEntity<List<OrderDto>> responseEntity = restTemplate.exchange(finalUrl.toString(), HttpMethod.GET,
                getHeaders(), new ParameterizedTypeReference<List<OrderDto>>() {
                });
        return responseEntity.getBody();
    }

    public Integer getOrderCount(String email) {
        StringBuilder finalUrl = new StringBuilder(urlBase);
        finalUrl.append(API_V1_ORDERS_COUNT);
        finalUrl.append(email);

        ResponseEntity<Integer> responseEntity = restTemplate.exchange(finalUrl.toString(), HttpMethod.GET,
                getHeaders(), Integer.class);
        return responseEntity.getBody();
    }

    public Double getOrderTotalPrice(String email) {
        StringBuilder finalUrl = new StringBuilder(urlBase);
        finalUrl.append(API_V1_ORDERS_TOTAL_PRICE);
        finalUrl.append(email);

        ResponseEntity<Double> responseEntity = restTemplate.exchange(finalUrl.toString(), HttpMethod.GET,
                getHeaders(), Double.class);
        return responseEntity.getBody();
    }

    public OrderDto setOrderAddress(Long orderId, AddressDto addressDto) {
        StringBuilder finalUrl = new StringBuilder(urlBase);
        finalUrl.append(API_V1_ORDERS_ADDRESS);
        finalUrl.append(orderId);

        ResponseEntity<OrderDto> responseEntity = restTemplate.exchange(finalUrl.toString(), HttpMethod.PUT,
                getHeaders(addressDto), OrderDto.class, addressDto);
        return responseEntity.getBody();
    }

    private HttpEntity<String> getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        return entity;
    }

    private <T> HttpEntity<T> getHeaders(T orderId) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<T> entity = new HttpEntity<>(orderId, headers);
        return entity;
    }
}
