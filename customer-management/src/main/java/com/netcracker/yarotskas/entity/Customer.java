package com.netcracker.yarotskas.entity;

import com.netcracker.yarotskas.dto.CustomerDto;
import lombok.Data;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

@Data
@Entity
@Table(name = "customers")
public class Customer extends BaseEntity{

//    @Basic(optional = false)
    private String name;

    private String surname;

    private String email;

    private int age;

    public Customer() {
    }

    public Customer(String name, String surname, String email, int age) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.age = age;
    }

    private Customer(CustomerDto customerDto) {
        this.name = customerDto.getName();
        this.surname = customerDto.getSurname();
        this.email = customerDto.getEmail();
        this.age = customerDto.getAge();
    }

    public static Customer convertFromDto(CustomerDto customerDto) {
        return new Customer(customerDto);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return Objects.equals(name, customer.name)
                && Objects.equals(surname, customer.surname)
                && Objects.equals(age, customer.age);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name, surname, age);
    }


}
