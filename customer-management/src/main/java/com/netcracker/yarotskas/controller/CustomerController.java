package com.netcracker.yarotskas.controller;

import com.netcracker.yarotskas.dto.CustomerDto;
import com.netcracker.yarotskas.entity.Customer;
import com.netcracker.yarotskas.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(value = "api/v1/customers")
public class CustomerController {

    private final CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping(path = "/{id}")
    public CustomerDto findById(@PathVariable("id") Long id) {
        return customerService.findById(id);
    }

    @GetMapping(path = "/authenticate/{email}")
    public CustomerDto findByEmail(@PathVariable("email") String email) {
        return customerService.findByEmail(email);
    }

    @GetMapping
    public Iterable<CustomerDto> findAll() {
        return customerService.findAll();
    }

    @PostMapping
    public CustomerDto create(@RequestBody @Validated CustomerDto customerDto) {
        return customerService.create(customerDto);
    }

    @PutMapping
    public CustomerDto update(@RequestBody @Validated CustomerDto customerToUpdate) {
        return customerService.update(customerToUpdate);
    }

    @DeleteMapping(path = "/{id}")
    public void deleteById(@PathVariable("id") Long id) {
        customerService.deleteById(id);
    }

}
