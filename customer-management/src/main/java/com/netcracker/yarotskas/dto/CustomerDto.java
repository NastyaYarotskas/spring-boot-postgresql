package com.netcracker.yarotskas.dto;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class CustomerDto implements BaseEntityDto {

    private Long id;

    @NotNull
    @Size(min = 1)
    private String name;

    private String surname;

    @Email
    @NotNull
    private String email;

    private int age;

}
