package com.netcracker.yarotskas.service.converter;

import com.netcracker.yarotskas.dto.CustomerDto;
import com.netcracker.yarotskas.entity.Customer;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public interface CustomerConverter {

    Customer convertFromDto(CustomerDto customerDto);

    CustomerDto convertFromEntity(Customer customer);

    default List createFromDtos(Collection<CustomerDto> dtos){
        return dtos.stream().map(this::convertFromDto).collect(Collectors.toList());
    }

    default List createFromEntities(Collection<Customer> entities) {
        return entities.stream().map(this::convertFromEntity).collect(Collectors.toList());
    }

}
