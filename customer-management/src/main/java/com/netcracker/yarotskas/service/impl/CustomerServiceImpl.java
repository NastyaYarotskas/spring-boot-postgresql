package com.netcracker.yarotskas.service.impl;

import com.netcracker.yarotskas.dto.CustomerDto;
import com.netcracker.yarotskas.entity.Customer;
import com.netcracker.yarotskas.exception.EntityNotFoundException;
import com.netcracker.yarotskas.repository.CustomerRepository;
import com.netcracker.yarotskas.service.CustomerService;
import com.netcracker.yarotskas.service.converter.CustomerConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;

    private final CustomerConverter customerConverter;

    @Autowired
    public CustomerServiceImpl(CustomerRepository customerRepository, CustomerConverter customerConverter) {
        this.customerRepository = customerRepository;
        this.customerConverter = customerConverter;
    }

    @Override
    public CustomerDto create(CustomerDto customerDto) {
        Customer customer = customerConverter.convertFromDto(customerDto);
        customer = customerRepository.save(customer);
        return customerConverter.convertFromEntity(customer);
    }

    @Override
    public CustomerDto findById(Long id) {
        Customer customer = customerRepository.findById(id).orElseThrow(NullPointerException::new);
        return customerConverter.convertFromEntity(customer);
    }

    @Override
    public List<CustomerDto> findAll() {
        return customerConverter.createFromEntities((List<Customer>) customerRepository.findAll());
    }

    @Override
    public CustomerDto update(CustomerDto customerDto) {
        Customer customer = customerConverter.convertFromDto(customerDto);
        customer = customerRepository.save(customer);
        return customerConverter.convertFromEntity(customer);
    }

    @Override
    public void deleteById(Long id) {
        try {
            customerRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }

    @Override
    public CustomerDto findByEmail(String email) {
        return customerConverter.convertFromEntity(customerRepository.findByEmail(email));
    }
}
