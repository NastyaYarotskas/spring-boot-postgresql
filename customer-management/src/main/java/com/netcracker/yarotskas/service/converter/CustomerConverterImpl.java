package com.netcracker.yarotskas.service.converter;

import com.netcracker.yarotskas.dto.CustomerDto;
import com.netcracker.yarotskas.entity.Customer;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class CustomerConverterImpl implements CustomerConverter {
    @Override
    public Customer convertFromDto(CustomerDto customerDto) {
        Customer customer = new Customer();
        BeanUtils.copyProperties(customerDto, customer);
        return customer;
    }

    @Override
    public CustomerDto convertFromEntity(Customer customer) {
        CustomerDto customerDto = new CustomerDto();
        BeanUtils.copyProperties(customer, customerDto);
        return customerDto;
    }
}
