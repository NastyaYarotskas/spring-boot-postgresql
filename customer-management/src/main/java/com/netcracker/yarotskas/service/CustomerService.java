package com.netcracker.yarotskas.service;

import com.netcracker.yarotskas.dto.CustomerDto;

import java.util.List;

public interface CustomerService {

    CustomerDto create(CustomerDto customerDto);

    CustomerDto findById(Long id);

    List<CustomerDto> findAll();

    CustomerDto update(CustomerDto customerDto);

    void deleteById(Long id);

    CustomerDto findByEmail(String email);
}
