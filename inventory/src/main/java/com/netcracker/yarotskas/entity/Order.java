package com.netcracker.yarotskas.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.google.common.base.Objects;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringJoiner;

@Data
@Entity
@Table(name = "orders")
public class Order extends BaseEntity{

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "order", cascade = CascadeType.ALL)
    private List<OrderItem> orderItems = new ArrayList<>();

    private OrderStatus status = OrderStatus.NEW;

    @OneToOne(mappedBy = "order", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private Address deliveryAddress;

    private Long customerId;

    private Double totalPrice;

    private Integer orderItemCount;

    private Date date;

    private Boolean isPaid;

    private String email;

    public Order() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return Objects.equal(orderItems, order.orderItems) &&
                Objects.equal(totalPrice, order.totalPrice) &&
                Objects.equal(orderItemCount, order.orderItemCount);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(orderItems, totalPrice, orderItemCount);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Order.class.getSimpleName() + "[", "]")
                .add("orderId=" + getId())
                .add("orderStatus=" + status)
                .add("orderItems=" + orderItems)
                .toString();
    }
}
