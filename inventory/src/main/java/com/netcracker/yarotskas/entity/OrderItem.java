package com.netcracker.yarotskas.entity;

import com.google.common.base.Objects;
import lombok.Data;

import javax.persistence.*;
import java.util.List;
import java.util.StringJoiner;

@Data
@Entity
public class OrderItem extends BaseEntity {

    @Basic(optional = false)
    private String name;

    @Basic(optional = false)
    private Integer quantity;

    @Basic(optional = false)
    private Long offerId;

    private String imageUrl;

    private String description;

    @OneToOne(mappedBy = "orderItem", cascade = CascadeType.ALL, orphanRemoval = true)
    private Price price;

    private String category;

    @ElementCollection
    private List<String> tags;

    @ManyToOne
    @JoinColumn(name = "order_id")
    private Order order;

    public OrderItem(){
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderItem orderItem = (OrderItem) o;
        return Objects.equal(quantity, orderItem.quantity) &&
                Objects.equal(offerId, orderItem.offerId) &&
                Objects.equal(price, orderItem.price) &&
                Objects.equal(order, orderItem.order);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(quantity, offerId, price, order);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", OrderItem.class.getSimpleName() + "[", "]")
                .add("quantity=" + quantity)
                .add("offerId=" + offerId)
                .add("price=" + price)
                .add("orderId=" + order.getId())
                .toString();
    }
}
