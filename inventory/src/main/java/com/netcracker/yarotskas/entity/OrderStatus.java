package com.netcracker.yarotskas.entity;

public enum OrderStatus {
    NEW, DELIVERED, REJECTED
}
