package com.netcracker.yarotskas.entity;

import lombok.Data;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import java.util.Objects;
import java.util.StringJoiner;

@Data
@Entity
public class Price extends BaseEntity {

    @Basic(optional = false)
    private Double value;

    @OneToOne
    OrderItem orderItem;

    public Price() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Price price = (Price) o;
        return Objects.equals(value, price.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), value);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Price.class.getSimpleName() + "[", "]")
                .add("value=" + value)
               // .addCategory("offerId=" + entity.getId())
                .toString();
    }
}
