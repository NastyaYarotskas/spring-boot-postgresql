package com.netcracker.yarotskas.entity;

import lombok.Data;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import java.util.Objects;

@Data
@Entity
public class Address extends BaseEntity {

    @Basic(optional = false)
    private String street;

    @Basic(optional = false)
    private String city;

    @OneToOne
    @JoinColumn(name = "order_id")
    private Order order;

    public Address() {
    }

    public Address(String street, String city) {
        this.street = street;
        this.city = city;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Address address = (Address) o;
        return Objects.equals(street, address.street) &&
                Objects.equals(city, address.city) &&
                Objects.equals(order, address.order);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), street, city, order);
    }

    @Override
    public String toString() {
        return "Address{" +
                "street='" + street + '\'' +
                ", city='" + city + '\'' +
                ", user=" + order +
                '}';
    }
}
