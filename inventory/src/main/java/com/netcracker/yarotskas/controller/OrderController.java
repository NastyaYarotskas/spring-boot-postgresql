package com.netcracker.yarotskas.controller;

import com.netcracker.yarotskas.dto.AddressDto;
import com.netcracker.yarotskas.dto.OrderDto;
import com.netcracker.yarotskas.dto.OrderItemDto;
import com.netcracker.yarotskas.entity.Order;
import com.netcracker.yarotskas.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "api/v1/orders")
public class OrderController {

    private final OrderService orderService;

    @Autowired
    public OrderController(OrderService customerService) {
        this.orderService = customerService;
    }

    @PostMapping
    public OrderDto create(@RequestBody OrderDto orderDto) {
        return orderService.create(orderDto);
    }

    @PutMapping
    public OrderDto update(@RequestBody OrderDto orderToUpdate) {
        return orderService.update(orderToUpdate);
    }

    @PutMapping(path = "/item")
    public OrderDto addItem(@RequestBody OrderItemDto orderItemDto) {
        return orderService.addItem(orderItemDto);
    }

    @PutMapping(path = "/pay")
    public OrderDto payForOrder(@RequestBody Long orderId) {
        return orderService.payForOrder(orderId);
    }

    @GetMapping
    public List<OrderDto> findAll() {
        return orderService.findAll();
    }

    @GetMapping(path = "/{id}")
    public OrderDto findById(@PathVariable("id") Long id) {
        return orderService.findById(id);
    }

    @DeleteMapping(path = "/{id}")
    public void deleteById(@PathVariable("id") Long id) {
        orderService.deleteById(id);
    }

    @GetMapping(path = "/email/{email}")
    public List<OrderDto> findByEmail(@PathVariable("email") String email) {
        return orderService.findByEmail(email);
    }

    @GetMapping(value = "/order/{paid}")
    private List<OrderDto> getOrderByPaidStatus(@PathVariable("paid") boolean paid) {
        return orderService.findByIsPaid(paid);
    }

    @DeleteMapping(path = "/order/{orderItemId}")
    public OrderDto deleteOrderItemById(@PathVariable("orderItemId") Long orderItemId) {
        return orderService.deleteOrderItemById(orderItemId);
    }

    @GetMapping(path = "/{customerId}/tag/{tag}")
    public List<OrderItemDto> findByTag(@PathVariable("customerId") Long customerId
            , @PathVariable("tag") List<String> tags) {
        return orderService.findByTags(customerId, tags);
    };

    @GetMapping(path = "/{customerId}/category/{category}")
    public List<OrderItemDto> findByCategory(@PathVariable("customerId") Long customerId
            , @PathVariable("category") String category) {
        return orderService.findByCategory(customerId, category);
    };

    @GetMapping(path = "/count/{email}")
    public Integer getOrderCount(@PathVariable("email") String email) {
        return orderService.getOrderCount(email);
    }

    @GetMapping(path = "/total/{email}")
    public Double getOrderTotalPrice(@PathVariable("email") String email) {
        return orderService.getTotalOrderPrice(email);
    }

    @PutMapping(path = "/address/{orderId}")
    public OrderDto setOrderAddress(@PathVariable("orderId") Long orderId, @RequestBody AddressDto addressDto) {
        return orderService.setOrderAddress(orderId, addressDto);
    }
}
