package com.netcracker.yarotskas.service;

import com.netcracker.yarotskas.dto.AddressDto;
import com.netcracker.yarotskas.dto.OrderDto;
import com.netcracker.yarotskas.dto.OrderItemDto;
import com.netcracker.yarotskas.entity.Address;

import java.util.List;

public interface OrderService {

    OrderDto create(OrderDto orderDto);

    List<OrderDto> findAll();

    OrderDto findById(Long id);

    OrderDto update(OrderDto orderDto);

    OrderDto addItem(OrderItemDto orderItemDto);

    OrderDto payForOrder(Long orderId);

    void deleteById(Long id);

    List<OrderDto> findByEmail(String email);

    List<OrderDto> findByIsPaid(boolean isPaid);

    OrderDto deleteOrderItemById(Long orderItemId);

    List<OrderItemDto> findByCategory(Long customerId, String category);

    List<OrderItemDto> findByTags(Long customerId, List<String> tags);

    Integer getOrderCount(String email);

    Double getTotalOrderPrice(String email);

    OrderDto setOrderAddress(Long orderId, AddressDto addressDto);
}
