package com.netcracker.yarotskas.service.impl;

import com.netcracker.yarotskas.dto.AddressDto;
import com.netcracker.yarotskas.dto.OrderDto;
import com.netcracker.yarotskas.dto.OrderItemDto;
import com.netcracker.yarotskas.entity.Address;
import com.netcracker.yarotskas.entity.Order;
import com.netcracker.yarotskas.entity.OrderItem;
import com.netcracker.yarotskas.exception.EntityNotFoundException;
import com.netcracker.yarotskas.repository.OrderItemRepository;
import com.netcracker.yarotskas.repository.OrderRepository;
import com.netcracker.yarotskas.service.OrderService;
import com.netcracker.yarotskas.service.converter.OrderConverter;
import com.netcracker.yarotskas.service.converter.OrderItemConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    private final OrderItemRepository orderItemRepository;

    private final OrderConverter orderConverter;

    private final OrderItemConverter orderItemConverter;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository, OrderItemRepository orderItemRepository, OrderConverter orderConverter, OrderItemConverter orderItemConverter) {
        this.orderRepository = orderRepository;
        this.orderItemRepository = orderItemRepository;
        this.orderConverter = orderConverter;
        this.orderItemConverter = orderItemConverter;
    }

    @Override
    public OrderDto create(OrderDto orderDto) {
        Order order = orderConverter.convertFromDto(orderDto);
        order = orderRepository.save(order);
        return orderConverter.convertFromEntity(order);
    }

    @Override
    public List<OrderDto> findAll() {
        List<Order> result = (List<Order>) orderRepository.findAll();
        return orderConverter.createFromEntities(result);
    }

    @Override
    public OrderDto findById(Long id) {
        Order order = orderRepository.findById(id)
                .orElseThrow(NullPointerException::new);
        return orderConverter.convertFromEntity(order);
    }

    @Override
    public OrderDto update(OrderDto orderDto) {
        Order order = orderConverter.convertFromDto(orderDto);
        return orderConverter.convertFromEntity(orderRepository.save(order));
    }

    @Override
    public OrderDto addItem(OrderItemDto orderItemDto) {
        Order order = orderRepository.findById(orderItemDto.getOrderId())
                .orElseThrow(NullPointerException::new);
        OrderItem orderItem = orderItemConverter.convertFromDto(orderItemDto);
        order.getOrderItems().add(orderItem);
        order.setTotalPrice(order.getTotalPrice() +
                orderItem.getQuantity() * orderItem.getPrice().getValue());
        order.setOrderItemCount(order.getOrderItemCount() + orderItem.getQuantity());
        orderItem.setOrder(order);
        return orderConverter.convertFromEntity(orderRepository.save(order));
    }

    @Override
    public OrderDto payForOrder(Long orderId) {
        Order order = orderRepository.findById(orderId)
                .orElseThrow(NullPointerException::new);
        order.setIsPaid(true);
        return orderConverter.convertFromEntity(order);
    }

    @Override
    public void deleteById(Long id) {
        try {
            orderRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }

    @Override
    public List<OrderDto> findByEmail(String email) {
        List<Order> orders = orderRepository.findByEmail(email);
        return orderConverter.createFromEntities(orders);
    }

    @Override
    public List<OrderDto> findByIsPaid(boolean isPaid) {
        List<Order> orders = orderRepository.findByIsPaid(isPaid);
        return orderConverter.createFromEntities(orders);
    }

    @Override
    public OrderDto deleteOrderItemById(Long orderItemId) {

        OrderItem orderItem = orderItemRepository.findById(orderItemId)
                .orElseThrow(NullPointerException::new);

        Order order = orderItem.getOrder();
        if(!order.getIsPaid()) {
            order.setOrderItemCount(order.getOrderItemCount() - orderItem.getQuantity());
            order.setTotalPrice(order.getTotalPrice() - orderItem.getPrice().getValue() * orderItem.getQuantity());

            order.getOrderItems().remove(orderItem);
            orderItemRepository.delete(orderItem);

            order = orderRepository.save(order);
        }

        return orderConverter.convertFromEntity(order);
    }

    @Override
    public List<OrderItemDto> findByCategory(Long customerId, String category) {
        List<Order> orders = orderRepository.findByCustomerId(customerId);
        List<OrderItem> result = new LinkedList<>();

        for (Order order: orders) {
            result.addAll(orderItemRepository.findByOrderAndCategory(order, category));
        }

        return orderItemConverter.createFromEntities(result);
    }

    @Override
    public List<OrderItemDto> findByTags(Long customerId, List<String> tags) {
        List<Order> orders = orderRepository.findByCustomerId(customerId);
        List<OrderItem> result = new LinkedList<>();

        for (Order order: orders) {
            result.addAll(orderItemRepository.findByOrderAndTags(order, tags));
        }

        return orderItemConverter.createFromEntities(result);
    }

    @Override
    public Integer getOrderCount(String email) {
        List<OrderDto> orders = findByEmail(email);
        return orders.size();
    }

    @Override
    public Double getTotalOrderPrice(String email) {
        List<OrderDto> orderDtos = findByEmail(email);
        Double totalPrice = 0.0;
        for (OrderDto orderDto : orderDtos) {
            totalPrice += orderDto.getTotalPrice();
        }
        return totalPrice;
    }

    @Override
    public OrderDto setOrderAddress(Long orderId, AddressDto addressDto) {
        Order order = orderRepository.findById(orderId).orElseThrow(NullPointerException::new);
        order.setDeliveryAddress(new Address(addressDto.getCity(), addressDto.getStreet()));
        return orderConverter.convertFromEntity(orderRepository.save(order));
    }
}
