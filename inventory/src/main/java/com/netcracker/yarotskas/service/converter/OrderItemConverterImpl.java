package com.netcracker.yarotskas.service.converter;

import com.netcracker.yarotskas.dto.OrderItemDto;
import com.netcracker.yarotskas.entity.OrderItem;
import com.netcracker.yarotskas.entity.Price;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class OrderItemConverterImpl implements OrderItemConverter {

    @Override
    public OrderItem convertFromDto(OrderItemDto orderItemDto) {
        OrderItem orderItem = new OrderItem();
        Price price = new Price();
        price.setValue(orderItemDto.getPrice());
        BeanUtils.copyProperties(orderItemDto, orderItem, "orderId");
        price.setOrderItem(orderItem);
        orderItem.setPrice(price);
        return orderItem;
    }

    @Override
    public OrderItemDto convertFromEntity(OrderItem orderItem) {
        OrderItemDto orderItemDto = new OrderItemDto();
        orderItemDto.setOrderId(orderItem.getOrder().getId());
        orderItemDto.setPrice(orderItem.getPrice().getValue());
        BeanUtils.copyProperties(orderItem, orderItemDto, "price");
        return orderItemDto;
    }
}
