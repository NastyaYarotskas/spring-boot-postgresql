package com.netcracker.yarotskas.service.converter;

import com.netcracker.yarotskas.dto.OrderDto;
import com.netcracker.yarotskas.entity.Order;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public interface OrderConverter {

    Order convertFromDto(OrderDto orderDto);

    OrderDto convertFromEntity(Order order);

    default List createFromDtos(Collection<OrderDto> dtos){
        return dtos.stream().map(this::convertFromDto).collect(Collectors.toList());
    }

    default List createFromEntities(Collection<Order> entities) {
        return entities.stream().map(this::convertFromEntity).collect(Collectors.toList());
    }

}
