package com.netcracker.yarotskas.service.converter;

import com.netcracker.yarotskas.dto.OrderDto;
import com.netcracker.yarotskas.dto.OrderItemDto;
import com.netcracker.yarotskas.entity.Order;
import com.netcracker.yarotskas.entity.OrderItem;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;


public interface OrderItemConverter {

    OrderItem convertFromDto(OrderItemDto orderItemDto);

    OrderItemDto convertFromEntity(OrderItem orderItem);

    default List createFromDtos(Collection<OrderItemDto> dtos){
        return dtos.stream().map(this::convertFromDto).collect(Collectors.toList());
    }

    default List createFromEntities(Collection<OrderItem> entities) {
        return entities.stream().map(this::convertFromEntity).collect(Collectors.toList());
    }

}
