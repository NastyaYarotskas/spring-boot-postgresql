package com.netcracker.yarotskas.service.converter;

import com.netcracker.yarotskas.dto.AddressDto;
import com.netcracker.yarotskas.dto.OrderDto;
import com.netcracker.yarotskas.dto.OrderItemDto;
import com.netcracker.yarotskas.entity.Address;
import com.netcracker.yarotskas.entity.Order;
import com.netcracker.yarotskas.entity.OrderItem;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class OrderConverterImpl implements OrderConverter {

    OrderItemConverter orderItemConverter = new OrderItemConverterImpl();

    @Override
    public Order convertFromDto(OrderDto orderDto) {
        Order order = new Order();
        Address address = new Address();
        BeanUtils.copyProperties(orderDto.getDeliveryAddress(), address);
        BeanUtils.copyProperties(orderDto, order, "orderItems", "deliveryAddress");

        if (orderDto.getOrderItems() != null) {
            List<OrderItem> orderItems = orderDto.getOrderItems().stream()
                    .map(orderItemDto -> {
                        OrderItem orderItem = orderItemConverter.convertFromDto(orderItemDto);
                        orderItem.setOrder(order);
                        return orderItem;
                    })
                    .collect(Collectors.toList());

            order.setOrderItems(orderItems);
        }

        address.setOrder(order);
        order.setDeliveryAddress(address);
        return order;
    }

    @Override
    public OrderDto convertFromEntity(Order order) {
        AddressDto addressDto = new AddressDto();
        BeanUtils.copyProperties(order.getDeliveryAddress(), addressDto);

        OrderDto orderDto = new OrderDto();
        BeanUtils.copyProperties(order, orderDto, "orderItems", "deliveryAddress");

        if(order.getOrderItems() != null) {
            List<OrderItemDto> orderItemDtos = order.getOrderItems().stream()
                    .map(orderItem -> {
                        OrderItemDto orderItemDto = orderItemConverter.convertFromEntity(orderItem);
                        return orderItemDto;
                    })
                    .collect(Collectors.toList());

            orderDto.setOrderItems(orderItemDtos);
        }

        orderDto.setStatus(order.getStatus().toString());
        orderDto.setDeliveryAddress(addressDto);
        return orderDto;
    }

}
