package com.netcracker.yarotskas.dto;

import lombok.Data;

@Data
public class AddressDto implements BaseEntityDto {

    private Long id;

    private String street;

    private String city;

}
