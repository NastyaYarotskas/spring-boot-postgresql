package com.netcracker.yarotskas.dto;

import lombok.Data;

import java.util.List;

@Data
public class OrderItemDto implements BaseEntityDto{

    private Long id;

    private String name;

    private Integer quantity;

    private Long offerId;

    private String imageUrl;

    private String description;

    private Double price;

    private String category;

    private List<String> tags;

    //new
    private Long orderId;
}
