package com.netcracker.yarotskas.dto;

import com.netcracker.yarotskas.entity.Address;
import com.netcracker.yarotskas.entity.BaseEntity;
import com.netcracker.yarotskas.entity.OrderItem;
import com.netcracker.yarotskas.entity.OrderStatus;
import lombok.Data;
import lombok.Getter;

import javax.persistence.CascadeType;
import javax.persistence.OneToOne;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class OrderDto implements BaseEntityDto {

    private Long id;

    private List<OrderItemDto> orderItems;

    private String status;

    private AddressDto deliveryAddress;

    private Long customerId;

    private Double totalPrice;

    private Integer orderItemCount;

    private Date date;

    private Boolean isPaid;

    private String email;
}
