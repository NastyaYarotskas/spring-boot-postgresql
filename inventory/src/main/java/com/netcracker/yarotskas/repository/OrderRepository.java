package com.netcracker.yarotskas.repository;

import com.netcracker.yarotskas.entity.Order;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends CrudRepository<Order, Long> {

    List<Order> findByEmail(String email);

    List<Order> findByIsPaid(boolean isPaid);

    List<Order> findByCustomerId(Long customerId);
}
