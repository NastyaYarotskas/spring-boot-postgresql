package com.netcracker.yarotskas.repository;

import com.netcracker.yarotskas.entity.Order;
import com.netcracker.yarotskas.entity.OrderItem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderItemRepository extends CrudRepository<OrderItem, Long>  {

    Order deleteOrderItemById(Long id);

    List<OrderItem> findByOrderAndCategory(Order order, String category);

    List<OrderItem> findByOrderAndTags(Order order, List<String> tags);
}
